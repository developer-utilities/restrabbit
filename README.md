# Usage

Have a look at the docker-compose.yml file. This has two services; the latest rabbit mq image, and the latest image of this repo.

An example of injecting data into the queue:

```
curl -X POST \
  http://localhost:80/api/v2/rabbitmq \
  -H 'Content-Type: application/json' \
  -d '{
    "id": 16971,
    "name" : "Jim"
  }'
```

# Future functionality

- Configurable queues.
    - Config already exists, I just need to fully document it, and improve it a little