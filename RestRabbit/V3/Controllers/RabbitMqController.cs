﻿// --------------------------------------------------------------------------------------------------
// <copyright file="RabbitMqController.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestRabbit.Domain;
using RestRabbit.Domain.Model;
using RestRabbit.Transform;

namespace RestRabbit.V3.Controllers
{
    [Route("api/v3/rabbitmq")]
    [ApiController]
    public class RabbitMqController : Controller
    {
        private readonly ILogger<RabbitMqController> _logger;
        private readonly RabbitMqPublisher _publisher;
        private readonly ITransformer<Payload, Model.Payload> _transformer;

        public RabbitMqController(ILogger<RabbitMqController> logger,
            RabbitMqPublisher publisher,
            ITransformer<Payload, Model.Payload> transformer)
        {
            _logger = logger;
            _publisher = publisher;
            _transformer = transformer;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Model.Payload input)
        {
            _logger.LogInformation("Received message...");
            var result = JsonConvert.SerializeObject(input);
            _logger.LogDebug($"message is : {result}");

            var internalModel = _transformer.Transform(input);
            _publisher.Publish(internalModel);
            return Ok();
        }

        [HttpGet]
        public IActionResult Health()
        {
            return Ok("Hello world from RestRabbit v3!");
        }
    }
}