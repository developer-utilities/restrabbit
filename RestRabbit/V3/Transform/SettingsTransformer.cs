﻿// --------------------------------------------------------------------------------------------------
// <copyright file="SettingsTransformer.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using RestRabbit.Domain.Model;
using RestRabbit.Transform;

namespace RestRabbit.V3.Transform
{
    public class SettingsTransformer : ITransformer<Settings, Model.Settings>
    {
        public Settings Transform(Model.Settings s)
        {
            var result = new Settings
            {
                Password = s.Password,
                ExchangeName = s.ExchangeName,
                ExchangeType = s.ExchangeType,
                HostName = s.HostName,
                UserName = s.UserName
            };

            return result;
        }
    }
}