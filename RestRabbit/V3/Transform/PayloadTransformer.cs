﻿// --------------------------------------------------------------------------------------------------
// <copyright file="PayloadTransformer.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using RestRabbit.Domain.Model;
using RestRabbit.Transform;

namespace RestRabbit.V3.Transform
{
    public class PayloadTransformer : ITransformer<Payload, Model.Payload>
    {
        public Payload Transform(Model.Payload s)
        {
            var result = new Payload
            {
                Settings = s.Settings,
                Data = s.Data
            };

            return result;
        }
    }
}