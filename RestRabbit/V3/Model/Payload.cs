﻿// --------------------------------------------------------------------------------------------------
// <copyright file="Payload.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using Newtonsoft.Json.Linq;

namespace RestRabbit.V3.Model
{
    public class Payload
    {
        public Domain.Model.Settings Settings { get; set; }
        public JObject Data { get; set; }
    }
}