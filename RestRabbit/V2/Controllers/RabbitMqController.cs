﻿// --------------------------------------------------------------------------------------------------
// <copyright file="RabbitMqController.cs" company="SITA INC Ireland Ltd">
//      Copyright © SITA INC Ireland Ltd 2018. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;

namespace RestRabbit.V2.Controllers
{
    [Route("api/v2/rabbitmq")]
    [ApiController]
    public class RabbitMqController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILogger<RabbitMqController> _logger;

        public RabbitMqController(ILogger<RabbitMqController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        [HttpPost]
        public void Post([FromBody] JObject input)
        {
            _logger.LogDebug("Message received: " + input);

            var hostName = _config["RabbitHost"];
            var userName = _config["RabbitUser"];
            var password = _config["RabbitPassword"];

            _logger.LogDebug($"RabbitMQ settings. HostName: [{hostName}]. UserName: [{userName}]");

            var factory = new ConnectionFactory
            {
                HostName = hostName,
                UserName = userName,
                Password = password
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare("exchange", "fanout");
                    _logger.LogInformation("Publishing message...");

                    var stringRepresentation = JsonConvert.SerializeObject(input);
                    var bytesRepresentation = Encoding.ASCII.GetBytes(stringRepresentation);

                    channel.BasicPublish("exchange", "", body: bytesRepresentation);
                }
            }
        }
    }
}