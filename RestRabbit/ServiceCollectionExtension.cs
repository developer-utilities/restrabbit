﻿// --------------------------------------------------------------------------------------------------
// <copyright file="ServiceCollectionExtension.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using Microsoft.Extensions.DependencyInjection;
using RestRabbit.Domain;
using RestRabbit.Domain.Model;
using RestRabbit.Transform;
using RestRabbit.V3.Transform;

namespace RestRabbit
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddComponents(this IServiceCollection services)
        {
            services.AddV3Transformation();
            services.AddSingleton<RabbitMqPublisher>();

            return services;
        }

        private static IServiceCollection AddV3Transformation(this IServiceCollection services)
        {
            services.AddSingleton<ITransformer<Payload, V3.Model.Payload>, PayloadTransformer>();
            services.AddSingleton<ITransformer<Settings, V3.Model.Settings>, SettingsTransformer>();

            return services;
        }
    }
}