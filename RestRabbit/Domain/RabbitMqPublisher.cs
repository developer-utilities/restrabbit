﻿// --------------------------------------------------------------------------------------------------
// <copyright file="RabbitMqPublisher.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RestRabbit.Domain.Model;

namespace RestRabbit.Domain
{
    public class RabbitMqPublisher
    {
        private readonly IConfiguration _config;
        private readonly ILogger<RabbitMqPublisher> _logger;

        private string _hostName;
        private string _userName;
        private string _password;
        private string _exchangeName;
        private string _exchangeType;

        public RabbitMqPublisher(IConfiguration config,
            ILogger<RabbitMqPublisher> logger)
        {
            _config = config;
            _logger = logger;
        }

        public void Publish(Payload payload)
        {
            Configure(payload.Settings);
            var factory = CreateConnectionFactory();

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(_exchangeName, _exchangeType);
                    _logger.LogInformation("Publishing message...");

                    var stringRepresentation = JsonConvert.SerializeObject(payload.Data);
                    var bytesRepresentation = Encoding.ASCII.GetBytes(stringRepresentation);

                    channel.BasicPublish(_exchangeName, "", body: bytesRepresentation);
                }
            }
        }

        private ConnectionFactory CreateConnectionFactory()
        {
            var factory = new ConnectionFactory
            {
                HostName = _hostName,
                UserName = _userName,
                Password = _password
            };
            return factory;
        }

        private void Configure(Settings settings)
        {
            _hostName = settings.HostName ?? _config["RabbitHost"];
            _userName = settings.UserName ?? _config["RabbitUser"];
            _password = settings.Password ?? _config["RabbitPassword"];
            _exchangeName = settings.ExchangeName;
            _exchangeType = settings.ExchangeType;

            _logger.LogDebug("RabbitMQ settings. " +
                             $"HostName: [{_hostName}]. " +
                             $"UserName: [{_userName}]." +
                             $"Password: [{_password}]." +
                             $"Exchange name: [{_exchangeName}]." +
                             $"Exchange type: [{_exchangeType}].");
        }
    }
}