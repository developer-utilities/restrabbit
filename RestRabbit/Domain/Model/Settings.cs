﻿// --------------------------------------------------------------------------------------------------
// <copyright file="Settings.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

namespace RestRabbit.Domain.Model
{
    public class Settings
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ExchangeName { get; set; } = "exchange";
        public string ExchangeType { get; set; } = "fanout";
    }
}