﻿// --------------------------------------------------------------------------------------------------
// <copyright file="ITransformer.cs" company="SITA INC Ltd">
//      Copyright © SITA INC Ltd 2019. Confidential. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------

namespace RestRabbit.Transform
{
    public interface ITransformer<out T, in S>
    {
        T Transform(S s);
    }
}